#| Generic Scheme mathematical library

Copyright (C) 2020 Aidan Nyquist
Copyright (C) 2016 Federico Beffa

This file is part of the Scheme mathematical library.

The Scheme mathematical library is free software; you can redistribute
it and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 3 of the
License, or (at your option) any later version.

The Scheme mathematical library is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the Scheme mathematical library; if not, write to the Free
Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
02110-1301, USA.

|#

;;; Code:

(library (scmutils generic)
  (export
   Sigma

   ;;;./scmutils/kernel/genenv.scm ; OK
   type type-predicate arity inexact? zero-like
   one-like identity-like zero? one? identity? negate invert
   square cube sqrt exp log exp2 exp10 log2 log10 sin cos tan
   cot sec csc asin acos sinh cosh tanh sech csch asinh acosh
   atanh abs determinant trace transpose dimension solve-linear
   derivative = < <= > >= + - * / dot-product cross-product
   outer-product expt gcd make-rectangular make-polar real-part
   imag-part magnitude angle conjugate atan partial-derivative
   partial apply arg-scale arg-shift sigma ref size compose

   ;;;./scmutils/poly/interp-generic ; OK
   Lagrange-interpolation-function

   ;;;./scmutils/poly/lagrange ; OK
   lagrange
   triangle-iterate
   make-linear-interpolator
   vector->vector-constructor

   ;;;./scmutils/units/hms-dms-radians
   degrees->radians radians->degrees xms->x x->xms dms->d d->dms dms->radians
   radians->dms hours->radians radians->hours hms->h h->hms
   hms->radians radians->hms

   ;;;./scmutils/units/convert.scm
   unit-convert

   ;;;./scmutils/mechanics/universal.scm ; OK
   D I D-as-matrix Taylor-series-coefficients)

  (import (except (rnrs base) error assert
                  inexact? zero? sqrt exp log sin cos tan
                  asin acos abs = < <= > >= + - * / expt gcd make-rectangular
                  make-polar real-part imag-part magnitude angle
                  atan apply)
          (rename (only (rnrs base) string<?) (string<? string:<?))
          (rnrs eval)
          (rnrs mutable-pairs)
          (rnrs io simple)
          (rnrs io ports)
          (rnrs r5rs)
          (rnrs conditions)
          (rnrs control)
          (rnrs syntax-case)
          (rename (except (rnrs lists) filter) (remq delq) (remv delv))
          (except (mit core) assert) ; assert is defined in general/logic-utils.scm
          (except (mit arithmetic) conjugate)
          (mit list)
          (except (mit arity) procedure-arity) ; use version in apply-hook
          (mit apply-hook)
          (mit hash-tables)
          (mit environment)
          (mit streams)
          (only (srfi :1) reduce reduce-right delete any every lset-adjoin
                make-list append-map)
          (srfi :9)
          (srfi :14) ; char-set
          (rename (srfi :41) (stream-cons cons-stream)
                  (stream-fold stream-accumulate))
          (ice-9 curried-definitions)
          (only (system base compile)
		compile)
	  (only (guile)
		error warn eval-when
		include-from-path import
                last-pair list-head random
                iota reverse! vector-copy
		quotient sort interaction-environment
		with-output-to-string)
          (scmutils base))


(include-from-path "./scmutils/kernel/fbe2-genenv.scm")

(include-from-path "./scmutils/poly/interp-generic.scm")

(include-from-path "./scmutils/poly/lagrange.scm")

(include-from-path "./scmutils/units/hms-dms-radians.scm")

(include-from-path "./scmutils/units/convert.scm")

;;; **************************************

(include-from-path "./scmutils/mechanics/universal.scm")

(define (Sigma a b proc)
  (g:sigma proc a b))

)
