#| The Scheme mathematical library

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994,
1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005,
2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2016, 2019,
2020 Massachusetts Institute of Technology

This file is part of the Scheme mathematical library.

The Scheme mathematical library is free software; you can redistribute
it and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of the
License, or (at your option) any later version.

The Scheme mathematical library is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the Scheme mathematical library; if not, write to the Free
Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
02110-1301, USA.

|#

;;; Code:

;; (declare (usual-integrations))

;; I have no clue what to do

(define saved-repl-eval "nothing")

;; set to 'expression' to suppress simplification.
;; Initialized at end of library.
(define *scmutils/repl-simplifier* (lambda (x) x))

;; change 'simplify' to 'expression' to suppress simplification.
(define (scmutils/simplify object)
  (define (simplifiable object)
    (prepare-for-printing object *scmutils/repl-simplifier*
                                        ;simplify ; automatic simplification
                                        ;expression ; suppress simplification
                          ))
  (if (or (symbol? object)
          (list? object)
          (vector? object)
          (procedure? object))
      (begin
       (simplifiable object)
       *last-expression-printed*)
    object))

(define (scmutils/repl-print val)
  (unless (eq? val unspecific)
    (pp (scmutils/simplify val) (readline-port))
    ;;(newline)
    (flush-output-port (readline-port))))

(define (pple) (pp *last-expression-printed*))

(define (start-scmutils-print!)
  (readline-active)
  (readline scmutils/repl-print))

(define (stop-scmutils-print!)
  (readline saved-repl-print))

(define (display-expression)
  (if (or (undefined-value? *last-expression-printed*)
          (and (procedure? *last-expression-printed*)
               (not (operator? *last-expression-printed*))))
      *last-expression-printed*
    (internal-show-expression *last-expression-printed*)))

(define de display-expression)
